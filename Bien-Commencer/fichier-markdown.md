# Partie 1
## Sous-partie 1 : texte
Une phrase sans rien

_Une phrase en italique_

__Une phrase en gras__

Un lien vers [fun-mooc.fr](https://fun-mooc.fr)

Une ligne de `code`

## Sous-partie 2 : listes

**Liste à puce**

* item  
   * sous-item
   * sous-item
* item

**Liste numérotée**

1. item
2. item
3. item

